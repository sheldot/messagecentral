'use strict';
var loopback = require('loopback');

module.exports = function(Gatewaymessage) {
  Gatewaymessage.observe('after save', function(context, next) {
    var io = Gatewaymessage.app.io;

    // Emit event to everyone so they are updated
    io.sockets.emit('incoming:message', context.instance);
    next();
  });
};
