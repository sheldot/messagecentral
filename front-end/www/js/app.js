angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.constant("API_URLS", {
  BASE: 'http://0.0.0.0:3001/api/v1/GatewayMessages/'
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  // Connection point for all other states
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Compose state details
  .state('tab.compose', {
    url: '/compose',
    views: {
      'tab-compose': {
        templateUrl: 'templates/tab-compose.html',
        controller: 'ComposeCtrl'
      }
    }
  })

  // Messages state details
  .state('tab.messages', {
      url: '/messages',
      views: {
        'tab-messages': {
          templateUrl: 'templates/tab-messages.html',
          controller: 'MessagesCtrl'
        }
      }
    })

    // Messages Details state details
    .state('tab.message-detail', {
      url: '/messages/:messageId',
      views: {
        'tab-messages': {
          templateUrl: 'templates/message-detail.html',
          controller: 'MessageDetailCtrl'
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/compose');

});
