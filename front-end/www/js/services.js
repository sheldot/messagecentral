angular.module('starter.services', [])

//Here LoopBackAuth service must be provided as argument for authenticating the user
.factory('socket', function($rootScope){
    //Creating connection with server
    var socket = io.connect('http://localhost:3001');
    return {
      on: function (eventName, callbackFn) {
        socket.on(eventName, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            callbackFn.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callbackFn) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callbackFn) {
              callbackFn.apply(socket, args);
            }
          });
        })
      }
    };

})

.factory('Messages', function(API_URLS, $http) {
  var headers = {"Accept": "*/*", "Content-Type": "application/json"};

  var service = {
    getAll: function() {
      var url = API_URLS.BASE;

      return $http({
          method: 'GET',
          url: url,
          headers: headers
      });
    },
    getSpecific: function(messageId) {
      var url = API_URLS.BASE.concat(messageId);

      return $http({
          method: 'GET',
          url: url,
          headers: headers
      });
    },
    create: function(messageObject) {
      var url = API_URLS.BASE;

      return $http({
          method: 'POST',
          url: url,
          data: messageObject,
          headers: headers
      });
    },
    update: function(messageId, messageObject) {
      var url = API_URLS.BASE.concat(messageId);

      return $http({
          method: 'PUT',
          url: url,
          data: messageObject,
          headers: headers
      });
    },
    remove: function(messageId) {
      var url = API_URLS.BASE.concat(messageId);

      return $http({
          method: 'DELETE',
          url: url,
          headers: headers
      });
    }
  };

  return service
});
