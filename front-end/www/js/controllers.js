angular.module('starter.controllers', [])

.controller('ComposeCtrl', function($scope, socket, Messages) {
  $scope.message = {
    sender: '',
    text: ''
  };

  $scope.clickSendMessage = function() {
    var messageObject = {
      sender: $scope.message.sender,
      message: $scope.message.text
    };

    // Create message
    Messages.create(messageObject).then(function(response) {
      if (response.status === 200) {
        $scope.message.sender = '';
        $scope.message.text = '';
      }
    });
  };
})

.controller('MessagesCtrl', function($scope, socket, Messages) {
  $scope.messages = [];

  // Gather all the messages
  Messages.getAll().then(function(response) {
    if (response.status === 200) {
      // Valid request
      $scope.messages = response.data.map(function(currentMessageObject) {
        // Set a random image
        currentMessageObject.image = 'img/material'.concat(Math.ceil(Math.random()*4), '.jpg');
        return currentMessageObject;
      });
    }
  });

  socket.on('incoming:message', function (message) {
    message.image = 'img/material'.concat(Math.ceil(Math.random()*4), '.jpg');
    $scope.messages.push(message);
  });
})

.controller('MessageDetailCtrl', function($scope, $stateParams, Messages) {
  $scope.message = null;

  // Gather a specific messages
  Messages.getSpecific($stateParams.messageId).then(function(response) {
    if (response.status === 200) {
      // This is a valid message
      response.data.image = 'img/material'.concat(Math.ceil(Math.random()*4), '.jpg');
      $scope.message = response.data;
    }
  });
});
